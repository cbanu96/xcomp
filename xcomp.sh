#!/bin/bash

# Written by Banu Cristian
# Downloads, builds and installs into $HOME i686-gcc and -binutils.

# Display progress bar only with curl
# Credit to https://github.com/marascio/bash-tips-and-tricks
download()
{
    local url=$1
    echo -n "    "
    wget --progress=dot $url 2>&1 | grep --line-buffered "%" | \
        sed -u -e "s,\.,,g" | awk '{printf("\b\b\b\b%4s", $2)}'
    echo -ne "\b\b\b\b"
    echo " DONE"
}

mkdir -p ~/build-xcomp/
cd ~/build-xcomp/

# First things first: wget magic
download "ftp://ftp.gnu.org/gnu/binutils/binutils-2.24.tar.gz"
download "ftp://ftp.gnu.org/gnu/gcc/gcc-4.9.2/gcc-4.9.2.tar.gz"
download "https://gmplib.org/download/gmp/gmp-6.0.0a.tar.bz2"
download "http://www.mpfr.org/mpfr-current/mpfr-3.1.2.tar.gz"
download "ftp://ftp.gnu.org/gnu/mpc/mpc-1.0.2.tar.gz"

# Alright, now we have to extract everything

tar -xvzf binutils-2.24.tar.gz
tar -xvzf gcc-4.9.2.tar.gz
tar -xvzf mpfr-3.1.2.tar.gz
tar -xvzf mpc-1.0.2.tar.gz
tar jxf gmp-6.0.0a.tar.bz2

# Now, move mpc, mpfr and gmp to gcc dir

mv gmp-6.0.0 gcc-4.9.2/gmp
mv mpfr-3.1.2 gcc-4.9.2/mpfr
mv mpc-1.0.2 gcc-4.9.2/mpc

# Let's start building

PREFIX="$HOME/opt/cross"
TARGET=i686-elf

mkdir -p $PREFIX

mkdir build-binutils
mkdir build-gcc

cd build-binutils
../binutils-2.24/configure --target=$TARGET --prefix="$PREFIX" --with-sysroot --disable-nls --disable-werror
make
make install

cd ../build-gcc
../gcc-4.9.2/configure --target=$TARGET --prefix="$PREFIX" --disable-nls --without-headers --enable-language=c,c++
make all-gcc
make all-target-libgcc
make install-gcc
make install-target-libgcc

# Now, we should be kinda done... just link our ld, gcc and as to $HOME/bin

mkdir -p $HOME/bin
cd $HOME/bin

ln -s $HOME/opt/cross/bin/$TARGET-ld
ln -s $HOME/opt/cross/bin/$TARGET-as
ln -s $HOME/opt/cross/bin/$TARGET-gcc
ln -s $HOME/opt/cross/bin/$TARGET-g++
